#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function

import os

from waflib import Build
from waflib.Tools import waf_unit_test
import wafimport

VERSION = '4.0.2'
APPNAME = 'gop-cotesdex'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

auto_options = []
extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Wconversion',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wsign-conversion',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

bugs = [
    'bugs/bug00.c',
    'bugs/bug01.c'
]

examples = [
    'examples/arg-destructor.c',
    'examples/autohelp.c',
    'examples/exit-status.c',
    'examples/mem.c',
    'examples/multiple-tables.c',
    'examples/parse.c',
    'examples/prevent-exit.c',
    'examples/program-name.c',
    'examples/remaining.c',
    'examples/return.c',
    'examples/simple-parse.c',
    'examples/termwidth.c',
    'examples/usage.c',
    'examples/yesno.c',
    'examples/yesno-arg-desc.c'
]

def options(opt):
    opt.load('compiler_c')
    opt.load('gnu_dirs')
    opt.load('cotesdex')

def configure(conf):
    conf.load('compiler_c')
    conf.load('gnu_dirs')

    conf.env.append_unique('CFLAGS', '-std=gnu99')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    conf.check_cfg(
            package='gop-4',
            uselib_store='GOP',
            args='--cflags --libs')

    conf.load('cotesdex')

    install_dirs = [
        ('PREFIX', 'Install prefix'),
        ('DOCDIR', 'Documentation install dir'),
        ('HTMLDIR', 'HTML files install dir')
    ]

    print()
    print('General configuration')
    conf.summarize_auto_options()
    print()
    print('Install directories')
    for name,descr in install_dirs:
        conf.msg(descr, conf.env[name], color='CYAN')

def build(bld):
    bld(
        features = 'cotesdex_doc cotesdex_example',
        source   = examples,
        use      = ['GOP']
    )

def check(bld):
    try:
        del os.environ['COLUMNS']
    except KeyError:
        pass

    bld(
        features = 'cotesdex_test',
        source   = examples + bugs,
        use      = ['GOP']
    )
    bld.add_post_fun(waf_unit_test.summary)
    bld.add_post_fun(waf_unit_test.set_exit_code)

class CheckContext(Build.BuildContext):
    cmd = 'check'
    fun = 'check'
