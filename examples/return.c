/*
 * @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex
 */

/* @cotesdex title GOP return example */
/*
 * @cotesdex description
 * This example shows how the library responds to a callback returning
 * GOP_DO_RETURN.
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --print-string=Hello
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --print-string Hello
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  -p Hello
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  -pHello
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First --print-string Hello Second Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 2
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First --print-string=Hello Second Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 2
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First -p Hello Second Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 2
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First -pHello Second Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 2
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First Second --print-string=Hello Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 3
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First Second --print-string Hello Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 3
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First Second -p Hello Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 3
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  First Second -pHello Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 3
 *   argc = 4
 *   argv[1] = First
 *   argv[2] = Second
 *   argv[3] = Third
 *   argv[4] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --print-string
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *   @cotesdex content
 *   error
 *   ret = -1
 *   argc = 2
 *   argv[1] = --print-string
 *   argv[2] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --print-string --print-string
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   --print-string
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --print-string=Hello --print-string
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   ret = 1
 *   argc = 2
 *   argv[1] = --print-string
 *   argv[2] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  -ffpHello
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hello
 *   flag = 2
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  -ffp
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *   @cotesdex content
 *   error
 *   flag = 2
 *   ret = -1
 *   argc = 2
 *   argv[1] = -p
 *   argv[2] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: return [OPTION...]
 *
 *     -p, --print-string=STRING    Print a string and stop parsing options
 *     -f, --flag                   Set a flag
 *
 *   Help options:
 *     -?, --help                   Show this help message
 *         --usage                  Display brief usage message
 *   exit
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --help --print-string=Hello
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: return [OPTION...]
 *
 *     -p, --print-string=STRING    Print a string and stop parsing options
 *     -f, --flag                   Set a flag
 *
 *   Help options:
 *     -?, --help                   Show this help message
 *         --usage                  Display brief usage message
 *   exit
 *   ret = 1
 *   argc = 2
 *   argv[1] = --print-string=Hello
 *   argv[2] = NULL
 *   @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --usage -p Hello First Second Third
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: return [-f?] [-p|--print-string=STRING] [-f|--flag] [-?|--help] [--usage]
 *   exit
 *   ret = 1
 *   argc = 6
 *   argv[1] = -p
 *   argv[2] = Hello
 *   argv[3] = First
 *   argv[4] = Second
 *   argv[5] = Third
 *   argv[6] = NULL
 *   @cotesdex
 */

#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

static char * print_string;

static gop_return_t
cb_error(gop_t * gop)
{
    puts("error");
    return GOP_DO_RETURN;
}

static gop_return_t
cb_exit(gop_t * gop)
{
    puts("exit");
    return GOP_DO_RETURN;
}

static gop_return_t
cb_print_string(gop_t * gop)
{
    puts(print_string);
    return GOP_DO_RETURN;
}

int
main(int argc, char ** argv)
{
    int ret;
    int flag = 0;

    const gop_option_t options[] = {
        {"print-string", 'p', GOP_STRING, &print_string,
            &cb_print_string, "Print a string and stop parsing options",
            "STRING"},
        {"flag", 'f', GOP_NONE, &flag, NULL, "Set a flag", NULL},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_table(gop, NULL, options);
    gop_autohelp(gop);
    gop_aterror(gop, &cb_error);
    gop_atexit(gop, &cb_exit);
    ret = gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (flag) {
        printf("flag = %d\n", flag);
    }

    printf("ret = %d\n", ret);
    printf("argc = %d\n", argc);
    for (int i = 1; i <= argc; ++i) {
        if (argv[i] != NULL) {
            printf("argv[%d] = %s\n", i, argv[i]);
        } else {
            printf("argv[%d] = NULL\n", i);
        }
    }

    return (ret > 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
