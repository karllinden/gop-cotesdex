/*
 * @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex
 */

/* @cotesdex title An example of gop_new_mem */
/*
 * @cotesdex description
 * This examples demonstrates how `gop_new_mem` can be used.
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --string=hej
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 */

/*
 * @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

/* The block (header and data) that each allocation has. */
struct block_s
{
    unsigned id;
    size_t   size;
    char     data[];
};
typedef struct block_s block_t;
#define ptr_to_block(ptr) \
    (block_t *) ((char *)ptr - offsetof(block_t, data))

/* Number of blocks that are currently allocated. */
static unsigned alloc_count = 0;

/* The maximum block id that has been given. */
static unsigned max_block_id = 0;

static void *
mymalloc(size_t size)
{
    block_t * block = malloc(sizeof(block_t) + size);
    if (block != NULL) {
        alloc_count++;
        max_block_id++;
        block->id = max_block_id;
        block->size = size;
        printf("Allocated a new block with id %u and size %ld.\n",
                block->id, block->size);
    }
    return block->data;
}

static void
myfree(void * ptr)
{
    if (ptr != NULL) {
        block_t * block = ptr_to_block(ptr);
        printf("Freeing block %u of size %ld.\n",
                block->id, block->size);
        free(block);
        alloc_count--;
    }
}

static void *
myrealloc(void * ptr, size_t size)
{
    if (ptr == NULL) {
        return mymalloc(size);
    } else if (size == 0) {
        myfree(ptr);
        return NULL;
    } else {
        block_t * block;
        block_t * new_block;

        block = ptr_to_block(ptr);
        printf("Reallocating block %u from size %ld to size %ld.\n",
                block->id, block->size, size);
        new_block = realloc(block, sizeof(block_t) + size);
        if (new_block != NULL) {
            return new_block->data;
        } else {
            return NULL;
        }
    }
}

static char *
mystrdup(const char * str)
{
    size_t size;
    char * new;

    printf("Duplicating \"%s\".\n", str);
    size = strlen(str) + 1;
    new = mymalloc(size);
    if (new != NULL) {
        memcpy(new, str, size);
    }
    return new;
}

int
main(int argc, char ** argv)
{
    int          exit_status = EXIT_FAILURE;
    const char * string      = NULL;
    gop_t *      gop;

    const gop_option_t options[] = {
        {"string", 's', GOP_STRING, &string, NULL, "A string",
            "STRING"},
        GOP_TABLEEND
    };

    gop = gop_new_mem(&mymalloc, &myfree, &myrealloc, &mystrdup);
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, NULL, options);
    gop_autohelp(gop);
    gop_description(gop, "Print a string.");
    gop_add_usage(gop, "");
    gop_add_usage(gop, "{-s|--string} STRING");
    gop_add_usage(gop, "{-?|--help|--usage}");
    gop_extra_help(gop, "Some extra help...\n");
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (string) {
        printf("string = %s\n", string);
    } else {
        printf("String not given.\n");
    }

    if (alloc_count > 0) {
        fprintf(stderr, "There were memory leaks.\n");
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
