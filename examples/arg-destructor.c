/*
 * @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex
 */

/* @cotesdex title An example of argument destructing */
/*
 * @cotesdex description
 * This example shows how the program can use an argument destructor to
 * destruct arguments that have been dynamically allocated.
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  --string=Hello
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = Hello
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  --string Hello
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = Hello
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -s Hello
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = Hello
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -sHello
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = Hello
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -s Hello -s Goodbye
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = Goodbye
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  --integer 89
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 89
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  --integer=62
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 62
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -i 39
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 39
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -i14
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 14
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -iHello
 *  @cotesdex
 *  @cotesdex stdout
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -ff
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   flag = 2
 *   argc = 1
 *   copy[1] = NULL
 *   @cotesdex
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --flag=stuff
 *  @cotesdex
 *  @cotesdex stdout
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -fffsstuff First Second
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = stuff
 *   flag = 3
 *   argc = 3
 *   copy[1] = First
 *   copy[2] = Second
 *   copy[3] = NULL
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -fffs stuff First Second
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = stuff
 *   flag = 3
 *   argc = 3
 *   copy[1] = First
 *   copy[2] = Second
 *   copy[3] = NULL
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -fi 81
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 81
 *   flag = 1
 *   argc = 1
 *   copy[1] = NULL
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  -fi81
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 81
 *   flag = 1
 *   argc = 1
 *   copy[1] = NULL
 * @cotesdex
 */

/*
 * @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  First -ffiHello Second
 *  @cotesdex
 *  @cotesdex stdout
 * @cotesdex
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <gop.h>

static void
free_argv_copy(char ** copy)
{
    for (int i = 0; copy[i] != NULL; ++i) {
        free(copy[i]);
    }
    free(copy);
}

static char **
copy_argv(int argc, const char ** argv)
{
    char ** copy = calloc((size_t)argc + 1, sizeof(char *));
    if (copy == NULL) {
        perror("calloc");
        return NULL;
    }

    for (int i = 0; i < argc; ++i) {
        copy[i] = strdup(argv[i]);
        if (copy[i] == NULL) {
            perror("strdup");
            goto error;
        }
    }

    return copy;
error:
    free_argv_copy(copy);
    return NULL;
}

static void
arg_destructor(char * arg)
{
    free(arg);
}

/*
 * Do not forget that if an error occurs, then the library attempts to
 * exit the program, leaving memory unfreed. Thus, an atexit callback
 * performing this cleanup is needed.
 */
static gop_return_t
cb_exit(gop_t * gop)
{
    char ** copy = gop_get_pointer(gop);
    free_argv_copy(copy);
    return GOP_DO_EXIT;
}

int
main(int argc, const char ** argv)
{
    gop_t * gop;

    char *  string  = NULL;
    int     integer = -1;
    int     flag    = 0;

    const gop_option_t options[] = {
        {"string", 's', GOP_STRING, &string, NULL, NULL, NULL},
        {"integer", 'i', GOP_INT, &integer, NULL, NULL, NULL},
        {"flag", 'f', GOP_NONE, &flag, NULL, NULL, NULL},
        GOP_TABLEEND
    };

    char ** copy = copy_argv(argc, argv);
    if (copy == NULL) {
        return EXIT_FAILURE;
    }

    gop = gop_new();
    if (gop == NULL) {
        free_argv_copy(copy);
        return EXIT_FAILURE;
    }

    /*
     * Make the copy of argv available through the exit callback through
     * a program settable pointer.
     */
    gop_set_pointer(gop, copy);
    gop_atexit(gop, &cb_exit);

    gop_add_table(gop, NULL, options);
    gop_set_arg_destructor(gop, &arg_destructor);
    gop_parse(gop, &argc, copy);
    gop_destroy(gop);

    if (string != NULL) {
        printf("string = %s\n", string);
        free(string);
    }
    if (integer >= 0) {
        printf("integer = %d\n", integer);
    }
    if (flag) {
        printf("flag = %d\n", flag);
    }

    printf("argc = %d\n", argc);
    free(copy[0]);
    for (int i = 1; i <= argc; ++i) {
        printf("copy[%d] = %s\n", i,
               copy[i] != NULL ? copy[i] : "NULL");
        free(copy[i]);
    }
    free(copy);

    return EXIT_SUCCESS;
}
