# Cotesdex Files for Great Option Parsing

Author: Karl Linden

Report bugs to: https://gitlab.com/karllinden/gop-cotesdex/issues

Homepage: https://gitlab.com/karllinden/gop-cotesdex


## Description

This is the distribution of cotesdex files for the Great Option Parsing
library (GOP).

The `bugs` directory contains test cases for previous bugs in GOP, so
that these bugs never resurface. The files in `bugs` are only tests.

The `examples` directory contains examples that will give you an
overview of how GOP works. You can read the examples as they are in this
distribution with cotesdex tags or you can install them somewhere
stripping of the (somewhat noisy) tags. The examples also serve as
documentation and tests.

Since the cotesdex program uses GOP to parse options, using cotesdex to
generate tests would cause a circular dependency (which can be avoided
by a --tests flag, of course). Therefore this is a separate
distribution.


# Dependencies

Obviously both [gop](https://gitlab.com/karllinden/gop) and
[cotesdex](https://gitlab.com/karllinden/cotesdex) are needed.

To run the tests you need whatever cotesdex requires to run tests, so
referer to the `README.md` file in the cotesdex distribution, for those
dependencies.

If you want to build documentation you also need
[asciidoc](http://asciidoc.org) and
[source-highlight](https://www.gnu.org/software/src-highlite/source-highlight.html).

# Building, Testing and Installing

If you are building from Git, you must first run:
```shell
git submodule update --init
```

If you are building from a tarball or when you have run the above
command you can precede with the following steps.

 1. Run `./waf configure`. If you do not want to build documentation
    pass `--no-doc` on the command line and if you do not want to
    install examples upon pass `--no-examples`.  Refer to
    `./waf configure --help` for more options.
 2. Build documentation and examples using `./waf`.
 3. Build and run the tests using `./waf check`.
 4. Optionally install examples and documentation using `./waf install`.

If anything fails, please file a bug report at the above mentioned
address.
